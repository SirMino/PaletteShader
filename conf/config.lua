configuration = {
    ---------------------------
    --section 1
    ---------------------------
    image = "city.png",
    palette_start = 2,
    palette_total = 8,
    -- Section 2 --------------
    bool_val = false,
    int_val = 11
}

palettes = {}
palettes[0] = { -- https://lospec.com/palette-list/spanish-sunset
    colors=5,
    name="Spanish sunset",
    col1={r=95,g=47,b=69},
    col2={r=160,g=47,b=64},
    col3={r=253,g=114,b=78},
    col4={r=250,g=187,b=100},
    col5={r=245,g=221,b=188}
}
palettes[1] = { -- vernella
    colors=5,
    name="Vernella 5",
    col5={r=159,g=252,b=223},
    col4={r=255,g=224,b=153},
    col3={r=255,g=255,b=255},
    col2={r=20,g=155,b=133},
    col1={r=23,g=22,b=20}
}
palettes[2] = { -- https://lospec.com/palette-list/twilight-5
    colors=5,
    name="Twilight",
    col5={r=251,g=187,b=173},
    col4={r=238,g=134,b=149},
    col3={r=74,g=122,b=150},
    col2={r=51,g=63,b=88},
    col1={r=41,g=40,b=49}
}
palettes[3] = { -- https://lospec.com/palette-list/late-night-bath
    colors=5,
    name="Late Night Bath",
    col5={r=246,g=145,b=151},
    col4={r=255,g=196,b=184},
    col3={r=116,g=131,b=140},
    col2={r=91,g=93,b=112},
    col1={r=40,g=45,b=60}
}
palettes[4] = { -- https://lospec.com/palette-list/ink
    colors=5,
    name="Ink",
    col5={r=234,g=240,b=216},
    col4={r=150,g=162,b=179},
    col3={r=89,g=96,b=112},
    col2={r=65,g=58,b=66},
    col1={r=31,g=31,b=41}
}
palettes[5] = { -- https://lospec.com/palette-list/chocomilk-8
    colors=8,
    name="Chocomilk",
    col8={r=214,g=245,b=228},
    col7={r=247,g=255,b=197},
    col6={r=180,g=199,b=136},
    col5={r=138,g=137,b=105},
    col4={r=139,g=106,b=68},
    col3={r=98,g=85,b=76},
    col2={r=70,g=60,b=60},
    col1={r=49,g=38,b=41}
}
palettes[6] = {
    colors=8,
    name="Vernella 8",
    col8={r=231,g=224,b=211},
    col7={r=206,g=158,b=139},
    col6={r=195,g=111,b=145},
    col5={r=147,g=67,b=133},
    col4={r=83,g=57,b=118},
    col3={r=0,g=78,b=107},
    col2={r=0,g=40,b=56},
    col1={r=0,g=25,b=25}
}
palettes[7] = {
    colors=12,
    name="Vernella 12",
    col1={r=24,g=19,b=33},
    col2={r=17,g=42,b=68},
    col3={r=5,g=93,b=116},
    col4={r=5,g=148,b=152},
    col5={r=0,g=205,b=172},
    col6={r=79,g=251,b=223},
    col7={r=92,g=251,b=250},
    col8={r=230,g=143,b=80},
    col9={r=252,g=197,b=148},
    col10={r=249,g=248,b=166},
    col11={r=241,g=192,b=51},
    col12={r=255,g=250,b=50}
}
palettes[8] = {
    colors=10,
    name="Nightsky Bricks",
    col1={r=4,g=4,b=18},
    col2={r=14,g=12,b=21},
    col3={r=27,g=28,b=25},
    col4={r=36,g=32,b=52},
    col5={r=52,g=58,b=65},
    col6={r=77,g=60,b=78},
    col7={r=82,g=79,b=94},
    col8={r=106,g=106,b=97},
    col9={r=119,g=102,b=127},
    col10={r=162,g=139,b=134},
}

function GetPalette(n)
    print("*** LUA GetPalette");
    return palettes[n]
end

function InitGame()
    print("*** LUA InitGame")

    -- create chars
    lua_index = createDynamicObj(1, 218, 17)
    objs[lua_index] = { behaviour = coroutine.create(charAnim1) }
    InitTask(lua_index)

    lua_index = createDynamicObj(2, 191, 122)
    objs[lua_index] = { behaviour = coroutine.create(charAnim2) }
    InitTask(lua_index)
    
    lua_index = createDynamicObj(3, 7, 122)
    objs[lua_index] = { behaviour = coroutine.create(charAnim3) }
    InitTask(lua_index)
end

function InitTask(index)
    -- print("*** LUA InitTask ", index)
    if coroutine.status(objs[index].behaviour) ~= 'dead' then
        -- print("*** LUA let's resume coroutine ", index)
        coroutine.resume(objs[index].behaviour, index)
    end
end

objs = {}
local objs_size = 1
function createDynamicObj(type, x, y)
    lua_index = sendDynamicObject(type, x, y)
    lua_index = lua_index + 1
    return lua_index
end

function behav(index)

    -- print("*** LUA behav1")
    -- le azioni chiamate sono gestite dal codice c
    -- il comando yield interrompe l'esecuzione fino a nuove ordine
    -- se messo tra loop continuo crea ciclo di comportamento
    while true do
        sendLinearMove(20, 400, 5, index)
        coroutine.yield()
        sendLinearMove(570, 400, 5, index)
        coroutine.yield()
        sendLinearMove(570, 20, 2, index)
        coroutine.yield()
        sendLinearMove(20, 20, 8, index)
        coroutine.yield()
    end
end

-- 218,17    308,17
-- 191,122		261,122		261,165		130,165		130,128		152,128		152,122
-- 7,122		7,198		118,198, 	7,198

function charAnim1(index)
    while true do
        sendLinearMove(296, 17, 5, index)
        coroutine.yield()
        sendLinearMove(218, 17, 5, index)
        coroutine.yield()
    end
end

function charAnim2(index)
    while true do
        sendLinearMove(256, 122, 2, index)
        coroutine.yield()
        sendLinearMove(256, 154, 2, index)
        coroutine.yield()
        sendLinearMove(130, 154, 3, index)
        coroutine.yield()
        sendLinearMove(130, 132, 1, index)
        coroutine.yield()
        sendLinearMove(150, 132, 1, index)
        coroutine.yield()
        sendLinearMove(150, 122, 1, index)
        coroutine.yield()
        sendLinearMove(191, 122, 1, index)
        coroutine.yield()
    end
end

function charAnim3(index)
    while true do
        sendLinearMove(107, 122, 3, index)
        coroutine.yield()
        sendLinearMove(7, 188, 5, index)
        coroutine.yield()
        sendLinearMove(7, 122, 2, index)
        coroutine.yield()
    end
end

function upDown(index)
    while true do
        sendLinearMove(200, 40, 8, index)
        coroutine.yield()
        sendLinearMove(200, 480, 1, index)
        coroutine.yield()
    end
end
