#version 330

// Input vertex attributes (from vertex shader)
in vec2 fragTexCoord;
in vec4 fragColor;

// Input uniform values
uniform sampler2D texture0;
uniform ivec3 palette[32];
uniform int palette_colors_number;

// Output fragment color
out vec4 finalColor;

// NOTE: Add here your custom variables

float _rgb_fraction(float rgb_value)
{
    return rgb_value / 255;
}

vec3 _get_final_color(float color_value)
{
    vec3 color;
    float unit_fraction =  float(1) / float(palette_colors_number);

    for (int i = 0; i < palette_colors_number; i++)
    {
        if ((color_value >= unit_fraction * i) && (color_value < unit_fraction * (i + 1)))
        {
            color.r = _rgb_fraction(palette[i].r);
            color.g = _rgb_fraction(palette[i].g);
            color.b = _rgb_fraction(palette[i].b);
            break;
        }
    }
    
    /*
    for (int i = palette_colors_number - 1; i >= 0; i--)
    {
        if ((color_value >= unit_fraction * i) && (color_value < unit_fraction * (i + 1)))
        {
            color.r = _rgb_fraction(palette[i].r);
            color.g = _rgb_fraction(palette[i].g);
            color.b = _rgb_fraction(palette[i].b);
            break;
        }
    }
    */

    /*
    if (palette_colors_number == 5)
    {
        if (color_value > 0.8)
        {
            color.r = _rgb_fraction(palette[0].r);
            color.g = _rgb_fraction(palette[0].g);
            color.b = _rgb_fraction(palette[0].b);
        }
        if (color_value < 0.8)
        {
            color.r = _rgb_fraction(palette[1].r);
            color.g = _rgb_fraction(palette[1].g);
            color.b = _rgb_fraction(palette[1].b);
        }
        if (color_value < 0.6)
        {
            color.r = _rgb_fraction(palette[2].r);
            color.g = _rgb_fraction(palette[2].g);
            color.b = _rgb_fraction(palette[2].b);
        }
        if (color_value < 0.4)
        {
            color.r = _rgb_fraction(palette[3].r);
            color.g = _rgb_fraction(palette[3].g);
            color.b = _rgb_fraction(palette[3].b);
        }
        if (color_value < 0.2)
        {
            color.r = _rgb_fraction(palette[4].r);
            color.g = _rgb_fraction(palette[4].g);
            color.b = _rgb_fraction(palette[4].b);
        }
    }
    else // else is 8 for now
    {
        if (color_value > 0.875)
        {
            color.r = _rgb_fraction(palette[0].r);
            color.g = _rgb_fraction(palette[0].g);
            color.b = _rgb_fraction(palette[0].b);
        }
        if (color_value < 0.875)
        {
            color.r = _rgb_fraction(palette[1].r);
            color.g = _rgb_fraction(palette[1].g);
            color.b = _rgb_fraction(palette[1].b);
        }
        if (color_value < 0.75)
        {
            color.r = _rgb_fraction(palette[2].r);
            color.g = _rgb_fraction(palette[2].g);
            color.b = _rgb_fraction(palette[2].b);
        }
        if (color_value < 0.625)
        {
            color.r = _rgb_fraction(palette[3].r);
            color.g = _rgb_fraction(palette[3].g);
            color.b = _rgb_fraction(palette[3].b);
        }
        if (color_value < 0.5)
        {
            color.r = _rgb_fraction(palette[4].r);
            color.g = _rgb_fraction(palette[4].g);
            color.b = _rgb_fraction(palette[4].b);
        }
        if (color_value < 0.375)
        {
            color.r = _rgb_fraction(palette[5].r);
            color.g = _rgb_fraction(palette[5].g);
            color.b = _rgb_fraction(palette[5].b);
        }
        if (color_value < 0.25)
        {
            color.r = _rgb_fraction(palette[6].r);
            color.g = _rgb_fraction(palette[6].g);
            color.b = _rgb_fraction(palette[6].b);
        }
        if (color_value < 0.125)
        {
            color.r = _rgb_fraction(palette[7].r);
            color.g = _rgb_fraction(palette[7].g);
            color.b = _rgb_fraction(palette[7].b);
        }
    }
    */

    return color;
}

void main()
{
    // Calculate final fragment color
    vec4 pixelColor = texture(texture0, fragTexCoord);
    finalColor = vec4(_get_final_color(pixelColor.r), 1.0);
}
