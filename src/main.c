#include <stdlib.h>
#ifdef __linux__
    #include <raylib.h>
    #include <raymath.h>
    #include "lua.h"
    #include "lauxlib.h"
    #include "lualib.h"
#elif _WIN32
    #include "..\include\raylib.h"
    #include "..\include\raymath.h"
    #include "..\include\lua.h"
    #include "..\include\lauxlib.h"
    #include "..\include\lualib.h"
    //#include <unistd.h> // for sleep
#endif

static inline void _print_debug(const char * message)
{
    printf("*** C %s\n", message);
}

int palette_index;
int palette_total;
int *entities;
int entities_size = 0;
int create_entity()
{
    entities = realloc(entities, (entities_size + 1) * sizeof(int));
    entities[entities_size] = entities_size + 1;
    entities_size++;
    return entities[entities_size - 1];
}

struct IVec3
{
    int r, g, b;
};
typedef struct IVec3 IVector3;
typedef struct IVec3 CC;

struct palette
{
    int color_number;
    const char * name;
    CC * colors;
} Palette;

const char * _get_lua_string(lua_State *lua, const char *field_name)
{
    lua_getfield(lua, -1, field_name);
    const char *return_val = lua_tostring(lua, -1);
    lua_pop(lua, 1);
    return return_val;
}

int _get_lua_number(lua_State *lua, const char *field_name)
{
    lua_getfield(lua, -1, field_name);
    int return_val = lua_tointeger(lua, -1);
    lua_pop(lua, 1);
    return return_val;
}

typedef struct animated_object
{
    int type;
    Vector2 pos;
    Vector2 pos_start, pos_end;
    float time_total, time_elapsed;
    bool end;
} AnimatedObject;

AnimatedObject *anims;
int anims_size = 0;
int create_anim(int type, int x, int y)
{
    printf("*** C create_anim\n");

    anims = realloc(anims, (anims_size + 1) * sizeof(AnimatedObject));
    AnimatedObject o;
    o.pos = (Vector2) { x, y };
    o.pos_start = (Vector2) { x, y };
    o.type = type;

    anims[anims_size] = o;
    anims_size++;
    printf("*** C anims_size %i\n", anims_size);
    return anims_size - 1; //anims[anims_size - 1];
}

// LUA wrapper
int _create_anim(lua_State *L)
{
    int type = lua_tonumber(L, 1);
    int x = lua_tonumber(L, 2);
    int y = lua_tonumber(L, 3);
    lua_pushinteger(L, create_anim(type, x, y));
    return 1;
}

static inline int _get_color_value(lua_State *L, const char *col_name, const char *rgb)
{
    int value;
    lua_pushstring(L, col_name);
    lua_gettable(L, -2);
    lua_pushstring(L, rgb);
    lua_gettable(L, -2);
    value = lua_tointeger(L, -1);
    lua_pop(L, 2);

    return value;
}

CC _get_color(lua_State *L, const char *col_name)
{
    CC color;
    color.r = _get_color_value(L, col_name, "r");
    color.g = _get_color_value(L, col_name, "g");
    color.b = _get_color_value(L, col_name, "b");

    return color;

}

void _set_palette(lua_State *L, Shader *shader)
{
    lua_getglobal(L, "GetPalette");
    if (lua_isfunction(L, -1))
    {
        lua_pushnumber(L, palette_index);
        lua_pcall(L, 1, 1, 0);
        if (lua_istable(L, -1))
        {
            int palette_colors_number = _get_lua_number(L, "colors");
            Palette.color_number = palette_colors_number;
            Palette.name = _get_lua_string(L, "name");
            Palette.colors = malloc(sizeof(CC) * palette_colors_number);

            for (int i = 0; i < palette_colors_number; i++)
            {
                CC current_color = _get_color(L, TextFormat("%s%i", "col", i+1));
                Palette.colors[i].r = current_color.r;
                Palette.colors[i].g = current_color.g;
                Palette.colors[i].b = current_color.b;
                //_print_debug(TextFormat("%s%i r%i g%i b%i", "col", i+1, current_color.r, current_color.g, current_color.b));
            }
        }
    }

    const int palette_shader_loc = GetShaderLocation(*shader, "palette");
    const int colors_shader_loc = GetShaderLocation(*shader, "palette_colors_number");

    IVector3 shaderpalette2[Palette.color_number];
    for (int i = 0; i < Palette.color_number; i++)
    {
        shaderpalette2[i] = Palette.colors[i];
        //_print_debug(TextFormat("IVector da passare shader r:%i g%i b%i", shaderpalette2[i].r, shaderpalette2[i].g, shaderpalette2[i].b));
    }

    SetShaderValueV(*shader, palette_shader_loc, &shaderpalette2, SHADER_UNIFORM_IVEC3, Palette.color_number);
    SetShaderValue(*shader, colors_shader_loc, &Palette.color_number, SHADER_UNIFORM_INT);
}

void linear_move(int index, int x, int y, int time)
{
    // printf("*** C move %i\n", index);
    // move stuff
    int cindex = index - 1;
    anims[cindex].pos_end.x = (float) x;
    anims[cindex].pos_end.y = (float) y;
    anims[cindex].time_elapsed = 0;
    anims[cindex].time_total = time;
    anims[cindex].end = false;
}

// wrapper called by Lua
int _linear_move(lua_State *L)
{
    int x = lua_tonumber(L, 1);
    int y = lua_tonumber(L, 2);
    int time = lua_tonumber(L, 3);
    int index = lua_tointeger(L, 4);
    // pop?
    linear_move(index, x, y, time);
    return 0;
}

RenderTexture2D render_map;
void load_map()
{
    lua_State *map = luaL_newstate();
    luaL_openlibs(map);
    luaL_dofile(map, "res/maps/map2.lua");
    lua_pushvalue(map, -1);

    int map_width = _get_lua_number(map, "width");
    int map_height = _get_lua_number(map, "height");
    int tilesize = _get_lua_number(map, "tilewidth");
    int layer_size = _get_lua_number(map, "nextlayerid") - 1;

    //printf("map w%i h%i tilesize%i\n", map_width, map_height, tilesize);

    lua_getfield(map, -1, "layers");
    lua_rawgeti(map, -1, 1);

	int * map_levels = malloc(sizeof(int) * map_width * map_height);

    lua_getfield(map, -1, "data");
    for(int i = 0; i < map_width * map_height; i++) {
        lua_rawgeti(map, -1, i + 1);
        map_levels[i] = lua_tointeger(map, -1);
        lua_pop(map, 1);
    }
    
    lua_pop(map, 1);

    /*
	int ** map_levels = malloc((sizeof(int) * map_width * map_height) * layer_size);
    for (int i = 0; i < layer_size; i++)
    {
        //lua_rawgeti(map, -1, i + 1);
        //_print_debug(TextFormat("map layer name %s", _get_lua_string(map, "name")));
        
        lua_getfield(map, -1, "data");
        for(int j = 0; j < map_width * map_height; j++) {
            lua_rawgeti(map, -1, j + 1);
            //_print_debug(TextFormat("put in array %i %i", i, j));
            map_levels[i][j] = lua_tointeger(map, -2);
            lua_pop(map, 1);
        }
        
        lua_pop(map, 1);    
    }
    */

    /*
    lua_rawgeti(map, -1, 1);
	int * level = malloc(sizeof(int) * map_width * map_height);
    lua_getfield(map, -1, "data");
    for(int i = 0; i < map_width * map_height; i++ ) {
        lua_rawgeti(map, -1, i + 1);
        level[i] = lua_tointeger(map, -1);
        lua_pop(map, 1);
    }
    lua_pop(map, 1);    
    */   

    lua_close(map);

    int tiles_per_row = 16;
    Texture2D tileset = LoadTexture("res/imgs/sewer.png");
    render_map = LoadRenderTexture(320, 240);
    BeginTextureMode(render_map);
        for (int i = 0; i <  map_width * map_height; i++)
        {
            int row = (int) map_levels[i] / tiles_per_row;
            int rectangle_x = (map_levels[i] - (tiles_per_row * row)) * tilesize - tilesize;
            int rectangle_y = row * tilesize;
            //_print_debug(TextFormat("tile:%i x:%i y:%i", map_levels[i], rectangle_x, rectangle_y));
            Rectangle source = (Rectangle) {rectangle_x, rectangle_y, 16, 16};
            int position_x = (i - (((int) i / map_width) * map_width)) * tilesize;
            int position_y = (int) i / map_width * tilesize;
            //_print_debug(TextFormat("index %i x:%i y:%i", i, position_x, position_y));
            Vector2 position = (Vector2) {position_x, position_y};
            DrawTextureRec(tileset, source, position, WHITE);
        }

    EndTextureMode();

/*
    for (int i = 0; i <= layer_size; i++)
    {
        for (int j = 0; j < map_width*map_height; j++)
        {
            _print_debug(TextFormat("layer:%i indice:%i value%i", i, j, map_levels[i][j]));
        }
    }
*/
}

int main(void)
{
    const int screen_width = 320;
    const int screen_height = 208;
    const int screen_render_width = 960;
    const int screen_render_height = 624;

    //SetTraceLogLevel(LOG_ERROR);
    InitWindow(screen_render_width, screen_render_height, "Shader Palette");
    //SetWindowState(FLAG_WINDOW_RESIZABLE);
    SetTargetFPS(60);

    printf("*** C INIT\n");

    const char *img_name;

    lua_State *L = luaL_newstate();
    luaL_openlibs(L);

    lua_register(L, "sendDynamicObject", _create_anim);
    lua_register(L, "sendLinearMove", _linear_move);

    luaL_dofile(L, "conf/config.lua");

    load_map();

    // call init function
    lua_getglobal(L, "InitGame");
    lua_pcall(L, 0, 0, 0);

    lua_getglobal(L, "configuration");
    if (lua_istable(L, -1))
    {
        // Load image name from config file
        img_name = _get_lua_string(L, "image");
        palette_index = _get_lua_number(L, "palette_start");
        palette_total = _get_lua_number(L, "palette_total");
    }
    else
    {
        _print_debug("LUA NOT TABLE");
    }

	Texture2D texture = LoadTexture(TextFormat("res/imgs/%s", img_name));
    Texture2D char1 = LoadTexture("res/imgs/sprites/char1.png");
    Texture2D char2 = LoadTexture("res/imgs/sprites/char2.png");
    Texture2D char3 = LoadTexture("res/imgs/sprites/char3.png");
	Shader shader = LoadShader(0, "res/shaders/palette.fs");
    _set_palette(L, &shader);

	//RenderTexture2D renderTexture = LoadRenderTexture(1366,768);
    RenderTexture2D renderScreen = LoadRenderTexture(screen_width, screen_height);
    SetTextureFilter(render_map.texture, TEXTURE_FILTER_POINT);

	int pos_x = 10;
	int pos_y = 10;

	while (!WindowShouldClose())
	{
        float delta_time = GetFrameTime();
		if (IsKeyDown(KEY_LEFT) || IsKeyDown(KEY_A))
		{
			pos_x = pos_x - 10;
		}
		if (IsKeyDown(KEY_RIGHT) || IsKeyDown(KEY_D))
		{
			pos_x = pos_x + 10;
		}
		if (IsKeyDown(KEY_UP) || IsKeyDown(KEY_W))
		{
			pos_y = pos_y - 10;
		}
		if (IsKeyDown(KEY_DOWN) || IsKeyDown(KEY_S))
		{
			pos_y = pos_y + 10;
		}
        if (IsKeyPressed(KEY_P))
        {
            if (palette_index >= palette_total)
            {
                palette_index = 0;
            }
            else 
            {
                palette_index++;
            }
            _set_palette(L, &shader);
        }

        for (int i = 0; i < anims_size; i++)
        {
            //printf("*** C anims loop %i\n", i);
            if (!anims[i].end)
            {
                // linear interpolation
                anims[i].pos = Vector2Add(Vector2Scale(Vector2Subtract(anims[i].pos_end, anims[i].pos_start), (anims[i].time_elapsed / anims[i].time_total)), anims[i].pos_start);
                if (anims[i].time_elapsed >= anims[i].time_total)
                {
                    anims[i].pos = anims[i].pos_end;
                    anims[i].end = true;
                }
                else
                {
                    anims[i].time_elapsed += delta_time;
                }
            }
            else
            {
                anims[i].pos_start = anims[i].pos;
                lua_getglobal(L, "InitTask");
                if (lua_isfunction(L, -1))
                {
                    lua_pushinteger(L, i+1);
                    lua_pcall(L, 1, 0, 0);
                }
            }
        }

		BeginTextureMode(renderScreen); 
		    ClearBackground(RAYWHITE);
            //DrawTexture(render_map.texture, 0, 0, WHITE);
            DrawTextureRec(render_map.texture, (Rectangle) {0,0,render_map.texture.width, -render_map.texture.height}, (Vector2) {0,0}, WHITE);
		    //DrawRectangle(pos_x,pos_y,100,50,(Color){DARKGRAY.r, DARKGRAY.g, DARKGRAY.b, 220});

            // draw chars
            //DrawTexture(char1, 279, 32, WHITE);
            //DrawTexture(char2, 186, 144, WHITE);
            //DrawTexture(char3, 25, 142, WHITE);

            for (int i = 0; i < anims_size; i++)
            {
                //DrawRectangle(anims[i].pos.x, anims[i].pos.y,40, 40, GREEN);
                switch (anims[i].type) {
                    case 1:
                        DrawTexture(char1, anims[i].pos.x, anims[i].pos.y, WHITE);
                        break;
                    case 2:
                        DrawTexture(char2, anims[i].pos.x, anims[i].pos.y, WHITE);
                        break;
                    case 3:
                        DrawTexture(char3, anims[i].pos.x, anims[i].pos.y, WHITE);
                        break;
                }
            }
            DrawText(TextFormat("Palette: %s (%i)", Palette.name, Palette.color_number), 10, 190, 10, RAYWHITE);
		EndTextureMode();

		BeginDrawing();
		    ClearBackground(RAYWHITE);
		    BeginShaderMode(shader);
            //DrawTextureRec(renderTexture.texture, (Rectangle){0,0,(float)renderTexture.texture.width, (float)-renderTexture.texture.height}, (Vector2){0,0},WHITE);
            DrawTexturePro(renderScreen.texture, 
                    (Rectangle) {0,0,renderScreen.texture.width, -renderScreen.texture.height},
                    (Rectangle) {0,0,screen_render_width,screen_render_height},
                    (Vector2) {0,0},
                    0.0,
                    WHITE);
		    EndShaderMode();
		    //DrawText("RAYLIB", 190, 200, 20, LIGHTGRAY);

		EndDrawing();
	}

    // Unload resources
    UnloadShader(shader);
    UnloadTexture(texture);
    UnloadRenderTexture(renderScreen);
    lua_close(L);
    
	CloseWindow();

	return 0;
}
